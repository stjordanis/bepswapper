from pprint import pprint

import streamlit as st
import yaml

from engine.calculator import Calculator
from engine.helpers import handle_config, get_best_node
from engine.repository import Repository
from engine.helpers import load_style, show_title

from view.view_product import ViewProduct
from view.view_summary import ViewSummary

@st.cache
def get_config():
    config = yaml.load(open('config.yaml', 'r'), Loader=yaml.Loader)
    return handle_config(config)

if __name__ == '__main__':

    config = get_config()
    load_style(st, 'engine/style.css')

    show_title(st.sidebar, 'Pool Details')
    placeholders = dict(
        title=st.empty(),
        time=st.empty(),
        data=st.empty(),
    )
    repository = Repository(config)
    repository.SetStatus(get_best_node(config))

    st.sidebar.text_input('Pool Wallet', value=repository.GetStatus()['wallet'])
    st.sidebar.text_input('Address', value=repository.GetStatus()['address'])

    calculator = Calculator(config, repository)

    modes = {"Summary for all products": ViewSummary(config, placeholders, calculator)}
    for product in [x for x in config['products'] if x != 'RUNE']:
        modes[f'Details for {product}'] = ViewProduct(config, placeholders, calculator, product)

    show_title(st.sidebar, 'Operation Mode')
    mode = st.sidebar.selectbox('', list(modes.keys()))
    modes[mode].Run(mode)
