import copy
from collections import deque
import numpy as np

class Repository:

    def __init__(self, config):
        self._config = config

        self._summary = dict(
            time=None,
            data=dict()
        )

        for asset in config['products'].keys():
            self._summary['data'][asset] = dict()
            self._update_time = deque(maxlen=10)
            self._status = dict()
            self.Lock = None


    def SetStatus(self, data):
        self._status = data

    def GetStatus(self):
        return self._status

    def SetUpdateTime(self, time):
        self._update_time.append(time)

    def GetUpdateTime(self):
        return np.mean(self._update_time) if self._update_time else None

    def SetSummaryDex(self, asset, data):
        self._summary['data'][asset]['dex'] = copy.deepcopy(data)

    def SetSummaryThor(self, asset, data):
        self._summary['data'][asset]['thor'] = copy.deepcopy(data)

    def GetSummaryDex(self, asset):
        return self._summary['data'][asset].get('dex', None)

    def GetSummaryThor(self, asset):
        return self._summary['data'][asset].get('thor', None)