from http import HTTPStatus

import requests

URL = 'https://dex.binance.org'

class BinanceDex:

    def __init__(self, config):
        self._config = config

    def _api_request(self, endpoint, params=None):
        r = requests.get(URL + endpoint, params=params or dict())
        return r.json() if r.status_code == HTTPStatus.OK else None

    def GetLatest(self, symbol):
        data = self._api_request('/api/v1/ticker/24hr', dict(symbol=symbol))
        if isinstance(data, list):
            return data[0]

    def GetLatestPrice(self, symbol):
        data = self.GetLatest(symbol)
        if isinstance(data, dict):
            return float(data.get('weightedAvgPrice', None))