from pprint import pprint

import numpy as np

from engine.repository import Repository
from engine.updater import Updater

class Calculator:

    def __init__(self, config: dict, repository: Repository):
        self._config = config
        self._repo = repository

    def Update(self):
        Updater(self._config, self._repo).Run()

    def GetStatus(self):
        return self._repo.GetStatus()

    def GetThorDetails(self, product):
        return self._repo.GetSummaryThor(product)

    def GetDexDetails(self, product):
        return self._repo.GetSummaryDex(product)

    def GetSummary(self, product):

        assetDepth = float(self._repo.GetSummaryThor(product)['assetDepth']) / 100_000_000
        runeDepth = float(self._repo.GetSummaryThor(product)['runeDepth']) / 100_000_000

        pool_in_rune = runeDepth/assetDepth
        binance_rune_price = float(self._repo.GetSummaryDex('RUNE')['weightedAvgPrice']) * float(self._repo.GetSummaryDex('BNB')['weightedAvgPrice'])

        if product == 'USD':
            pool_in_usd = 1 / pool_in_rune
            binance_in_usd = binance_rune_price
            binance_in_rune = 1 / binance_rune_price
        elif product == 'BNB':
            pool_in_usd = pool_in_rune * binance_rune_price
            binance_in_usd = float(self._repo.GetSummaryDex('BNB')['weightedAvgPrice'])
            binance_in_rune = binance_in_usd / binance_rune_price
        else:
            pool_in_usd = pool_in_rune * binance_rune_price
            binance_in_usd = float(self._repo.GetSummaryDex(product)['weightedAvgPrice']) * float(self._repo.GetSummaryDex('BNB')['weightedAvgPrice'])
            binance_in_rune = binance_in_usd/binance_rune_price


        return dict(
            assetDepth=assetDepth,
            runeDepth=runeDepth,
            product=product,
            pool_in_usd=pool_in_usd,
            binance_in_usd=binance_in_usd,
            binance_in_rune=binance_in_rune,
            binance_ask=float(self._repo.GetSummaryDex(product)['askPrice']),
            binance_bid=float(self._repo.GetSummaryDex(product)['bidPrice']),
            pool_in_rune=pool_in_rune,
            delta_pct=100 * (binance_in_usd/pool_in_usd - 1),
            columns='product pool_in_rune pool_in_usd assetDepth runeDepth binance_in_usd delta_pct'.split(' '),
            )

    def GetCalcs(self, product: str):

        summary = self.GetSummary(product)

        assetDepth = summary['assetDepth']
        runeDepth = summary['runeDepth']
        price_diff = summary['binance_in_usd'] - summary['pool_in_usd']
        trade_size = (price_diff * assetDepth * runeDepth) / (5 * assetDepth)
        y = (trade_size * assetDepth * runeDepth)/pow(assetDepth + trade_size, 2)

        new_X = assetDepth + trade_size
        new_Y = runeDepth - y
        new_price = new_X/new_Y

        return dict(
            product=product,
            assetDepth=assetDepth,
            runeDepth=runeDepth,
            pool_in_usd=summary['pool_in_usd'],
            binance_in_usd=summary['binance_in_usd'],
            pool_in_rune=summary['pool_in_rune'],
            delta_pct=summary['delta_pct'],
            price_diff=price_diff,
            trade_size=trade_size,
            y=y,
            new_X=new_X,
            new_Y=new_Y,
            new_price=new_price,
            columns='product assetDepth runeDepth pool_in_usd binance_in_usd pool_in_rune delta_pct '
            'price_diff trade_size y new_X new_Y new_price'.split(' '))


    def _get_pnl(self, product, trade_size, force_market_price=1):

        product_data = self.GetSummary(product)
        rune_data = self.GetSummary('USD')
        bnb_data = self.GetSummary('BNB')

        pnls = []

        if product == 'USD':
            market_price_in_rune = 1 / (force_market_price * product_data['binance_in_usd'])
        else:
            market_price_in_rune = (force_market_price * product_data['binance_in_usd'])/ rune_data['binance_in_usd']

        X, Y = product_data['assetDepth'], product_data['runeDepth']

        def delta():
            return market_price_in_rune / (Y / X) - 1

        while True:

            # We limit delta (and swap size) to 100%

            _delta = delta()
            _delta = _delta if abs(_delta) < 1 else np.sign(_delta)

            x = -1 * delta() * X * trade_size
            y = x * X * Y / (x + X) ** 2

            poolUsdAfter = rune_data['binance_in_usd'] * (Y - y) / (X + x) if product != 'USD' else (X + x)/(Y - y)

            if x > 0:

                coeff = dict(
                    USD=1/bnb_data['binance_bid'],
                    BNB=1
                ).get(product, product_data['binance_ask'])

                reduced = y * X / Y
                slippage = 100 * ((x - reduced) / reduced)

                bnb = y * rune_data['binance_bid'] - x * coeff
                pnls.append(dict(pnl_usd=bnb * bnb_data["binance_in_usd"],
                                 amount=x,
                                 product=product,
                                 poolRune=Y / X,
                                 marketRune=market_price_in_rune,
                                 poolRuneAfter=(Y-y)/(X+x),
                                 poolUsdAfter=poolUsdAfter,
                                 take=y,
                                 reduced=reduced,
                                 slippage=slippage,
                                 delta_before=delta(),
                                 assetDepth=X,
                                 runeDepth=Y,
                                 newAssetDepth=X + x,
                                 newRuneDepth=Y-y))

            else:

                coeff = dict(
                    USD=1 / bnb_data['binance_ask'],
                    BNB=1,
                    ).get(product, product_data['binance_bid'])

                reduced = abs(y / (Y/X))
                slippage = 100 * ((reduced - abs(x)) / reduced)

                bnb = abs(x) * coeff - abs(y * rune_data['binance_ask'])
                pnls.append(dict(pnl_usd=bnb * bnb_data["binance_in_usd"],
                                 amount=abs(y),
                                 product='RUNE',
                                 poolRune=Y / X,
                                 marketRune=market_price_in_rune,
                                 poolRuneAfter=(Y-y)/(X+x),
                                 poolUsdAfter=poolUsdAfter,
                                 take=abs(x),
                                 reduced=reduced,
                                 slippage=slippage,
                                 delta_before=delta(),
                                 assetDepth=X,
                                 runeDepth=Y,
                                 newAssetDepth=X + x,
                                 newRuneDepth=Y - y))

                X, Y = X + x, Y - y

            if pnls:
                return dict(N=len(pnls), pnl_usd=sum(x['pnl_usd'] for x in pnls), delta=delta(), swap=pnls)
            else:
                return dict(N=0, pnl_usd=None, swap=None)

    def GetPnl(self, product: str, steps=5, force_market_price=1, trade_size=0.2):
            return self._get_pnl(product, trade_size, force_market_price)
