requests==2.23.0
numpy==1.18.3
pandas==1.0.3
PyYAML==5.3.1
streamlit==0.58.0
