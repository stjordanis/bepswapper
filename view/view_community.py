from engine.calculator import Calculator
from engine.helpers import show_title

class ViewCommunity:

    def __init__(self, config: dict, placeholders: dict, calculator: Calculator):
        self._config = config
        self._placeholders = placeholders
        self._calculator = calculator

    def Run(self, title):
        show_title(self._placeholders['title'], title)